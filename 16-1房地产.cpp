#include<iostream>
#include<cmath>
#include<iomanip>
using namespace std;

class Land {
public:
    Land():price_(0) {}
    explicit Land(int price) {
        price_ = price;
    }
    virtual double CalMoney() = 0;
protected:
    int price_;
};

class Square: public Land {
public:
    Square(double len, int price) {
        len_ = len;
        price_ = price;
    }
    double CalMoney() {
        return pow(len_, 2) * price_;
    }
private:
    double len_;
};

class Circle:public Land {
public:
    Circle(double radius, int price) {
        radius_ = radius;
        price_ = price;
    }
    double CalMoney() {
        return acos(-1) * pow(radius_, 2) * price_;
    }
private:
    double radius_;
};

class Accountant {
public:
    Accountant():money_(0) {}
    void DevelopEstate(Land* land) {
        money_ += land -> CalMoney();
    }
    double CheckMoney() {
        return money_;
    }
private:
    double money_;
};

int main() {
    Accountant py;
    Circle *a = new Circle(100, 10000);
    Square *b = new Square(100, 30000);
    py.DevelopEstate(a);
    cout << setprecision(10) << py.CheckMoney() << endl; //setprecisionÊÇ<iomanip>¿âÖÐµÄº¯Êý
    py.DevelopEstate(b);
    cout << setprecision(10) << py.CheckMoney() << endl;
    cout << "test4webhook" << endl;
    return 0;
}

