#include <iostream>
#include <string>
using namespace std;

class SongList {
	string owner;          // 歌单拥有者
	unsigned int size;         // 歌单中有效歌曲的数量
	unsigned int capacity;          // 歌单的最大容量
	string *songs;         // 歌曲列表，一个string数组
	
public:
	// 初始化的歌曲列表中会保证没有重复歌曲
	SongList(const string _owner, unsigned int _size, unsigned int _capacity, string *_songs);
	
	SongList(const SongList &songList);
	
	// 添加歌曲，请考虑去重的情况（如果歌曲已经存在则不添加）、歌单已经满了需要扩容的情况，扩容方案自选
	// 扩容方案1：原地扩容，C语言中有个realloc函数可以实现
	// 扩容方案2：重新开辟一个更大容量songs歌曲列表，并将当前的数据迁移过去
	void addSong(const string &song);
	
	// 删除指定歌曲，歌曲不存在则无动作
	void removeSong(const string &song);
	
	// 清空歌曲
	void clearSong();
	
	SongList &operator=(const SongList &songList);
	
	friend ostream &operator<<(ostream &os, const SongList &songList);
	
	~SongList();
};

SongList::SongList(string _owner, unsigned int _size, unsigned int _capacity, string *_songs) : owner(_owner),size(_size),capacity(_capacity){
	songs = new string[capacity];
	for (unsigned int i = 0; i < _size; ++i) {
		songs[i] = _songs[i];
	}
}

SongList::SongList(const SongList &songList) {
	size = songList.size;
	capacity = songList.capacity;
	owner = songList.owner;
	songs = new string[capacity];
	for (unsigned int i = 0; i < size; ++i) {
		songs[i] = songList.songs[i];
	}
}

void SongList::addSong(const string &song) {
	for (unsigned int i = 0; i < size; ++i) {
		if (songs[i] == song) {
			return;
		}
	}
	
	if (size >= capacity) {
		string *oldSongs = songs;
		capacity *= 2;
		songs = new string[capacity];
		for (unsigned int i = 0; i < size; ++i) {
			songs[i] = oldSongs[i];
		}
		delete[] oldSongs;
	}
	songs[size++] = song;
}

void SongList::removeSong(const string &song) {
	for (unsigned int i = 0; i < size; ++i) {
		if (song == songs[i]) {
			for (unsigned int j = i; j + 1 < size; ++j) {
				songs[j] = songs[j + 1];
			}
			--size;
		}
	}
}

void SongList::clearSong() {
	size = 0;
}

ostream &operator<<(ostream &os, const SongList &songList) {
	if (songList.size <= 0) {
		os << songList.owner << "'s SongList is Empty." << endl;
		return os;
	}
	cout << "owner:" << songList.owner << endl;
	cout << "Songs:" << endl;
	for (unsigned int i = 0; i < songList.size; ++i) {
		os << songList.songs[i] << std::endl;
	}
	return os;
}

SongList &SongList::operator=(const SongList &songList) {
	if (&songList != this) {
		delete[] songs;
		size = songList.size;
		capacity = songList.capacity;
		owner = songList.owner;
		songs = new string[capacity];
		for (unsigned int i = 0; i < size; ++i) {
			songs[i] = songList.songs[i];
		}
	}
	
	return *this;
}



SongList::~SongList(){
	delete [] songs;
}


int main() {
	int num;
	cin >> num;
	getchar();                  //读取换行符
	string name = "LiSi";
	string *songs = new string[num];
	for (int i = 0; i < num; ++i) {
		getline(cin, songs[i]);
	}
	string temp_song =  "两只老虎";
	
	SongList songList1(name, num, num, songs);
	songList1.addSong(temp_song);
	songList1.addSong(temp_song);
	delete []songs;
	SongList songList2(songList1);
	songList2 = songList2;
	songList2.removeSong(temp_song);
	
	SongList songList3 = songList2;
	songList3.clearSong();
	
	cout << songList1 << endl;
	cout << songList2 << endl;
	cout << songList3 << endl;
	cout << "test" << endl;
	
	return 0;
}
